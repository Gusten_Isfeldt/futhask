module Backends where

data Backend = C | Hip | OpenCL | Cuda | Multicore
