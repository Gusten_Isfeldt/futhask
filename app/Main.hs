module Main where

import System.IO
import Data.Maybe
import Data.List (intercalate)
import Debug.Trace
import System.Directory
import System.Environment
import CodeBodies
import Conversion
import Headers
import Backends

writeModule backend directory moduleName (subModuleName, headerF, body) 
    = writeFile fn string
    where fn = directory ++ "/" ++ moduleName 
            ++ (case subModuleName of Just n -> "/" ++ n; Nothing -> "") ++ ".hs"
          string = headerF backend moduleName subModuleName ++ body

main :: IO ()
main = do
    args <- getArgs
    [ backendS, headerName, srcDir, moduleName ] <- getArgs >>= \args -> case args of
         [a, b, c, d] -> return args
         _            -> error "futhask takes four arguments:\n - backend (c, opencl, cuda, hip, multicore)\n - Futhark header file\n - Haskell source directory\n - module name"
    backend <- case backendS of
        "c"         -> return C
        "opencl"    -> return OpenCL
        "cuda"      -> return Cuda
        "multicore" -> return Multicore
        "hip"       -> return Hip
        _           -> error $ "unknown backend: " ++ backendS ++ "\n  available backends: c, opencl, cuda, multicore, hip"
    header <- readHeader headerName
    
    createDirectoryIfMissing False (srcDir ++ "/" ++ moduleName)
    mapM_ (writeModule backend srcDir moduleName)
        [ (Just "Raw", rawHeader, rawImportString header)
        , (Just "Entries", entriesHeader, entryCallString header)
        , (Just "Types", typesHeader, instanceDeclarationString header)
        , (Just "TypeClasses", typeClassesHeader, typeClassesBody)
        , (Just "Context", contextHeader, contextBody)
        , (Just "Config", configHeader, configBody backend)
        , (Just "Fut", futHeader, futBody)
        , (Just "Wrap", wrapHeader, wrapBody)
        , (Just "Utils", utilsHeader, utilsBody)
        , (Nothing, exportsHeader, "") ]

